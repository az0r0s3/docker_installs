#!/bin/bash

clear
OS="$REPLY" ## <-- This $REPLY is about OS Selection
echo "We can install Docker-CE, Docker-Compose, NGinX Proxy Manager, and Portainer-CE."
echo "Please select 'y' for each item you would like to install."
echo "NOTE: Without Docker you cannot use Docker-Compose, NGinx Proxy Manager, or Portainer-CE."
echo "       You also must have Docker-Compose for NGinX Proxy Manager to be installed."
echo ""
echo ""

ISACT=$( (sudo systemctl is-active docker; docker compose version ) 2>&1 )

#### Try to check whether docker is installed and running - don't prompt if it is
    read -rp "Docker-CE (y/n): " DOCK
        
    if [[ "$DOCK" == [yY] ]]; then
        echo "##################################################"
        echo "###     Install Docker CE & Docker Compose     ###"
        echo "##################################################"

        echo "    1. Updating System Packages..."
        sudo yum check-update > ~/docker-script-install.log #2>&1
            
        echo "    2. Set up the repository..."
        sudo yum install -y yum-utils >> ~/docker-script-install.log #2>&1
        sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo >> ~/docker-script-install.log #2>&1

        if [[ "$ISACT" != "active" ]]; then
            echo "    3. Installing Docker-CE (Community Edition)..."
            sleep 2s
            sudo yum install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin -y >> ~/docker-script-install.log #2>&1

            echo "    4. Starting the Docker Service..."
            sleep 2s
            sudo systemctl start docker >> ~/docker-script-install.log #2>&1

            echo "    5. Enabling the Docker Service..."
            sleep 2s
            sudo systemctl enable docker >> ~/docker-script-install.log #2>&1

            echo "      - docker version is now:"
            DOCKERV=$(docker -v)
            echo "        "${DOCKERV}
            sleep 3s
        fi
    fi

    read -rp "NGinX Proxy Manager (y/n): " NPM

    if [[ "$NPM" == [yY] ]]; then
        echo "################################################"
        echo "######      Creating a Docker Network    #######"
        echo "################################################"

        sudo docker network create main-network 
        sleep 2s
        # move to home directory of user
        cd

        echo "##########################################"
        echo "###     Install NGinX Proxy Manager    ###"
        echo "##########################################"
    
        # pull an nginx proxy manager docker-compose file from github
        echo "    1. Pulling a default NGinX Proxy Manager docker-compose.yml file."

        mkdir -p docker/nginx-proxy-manager
        cd docker/nginx-proxy-manager

        curl https://gitlab.com/az0r0s3/docker_installs/-/raw/main/az0r0s3/docker_compose.nginx_proxy_manager.yml -o docker-compose.yml >> ~/docker-script-install.log #2>&1

        echo "    2. Running the docker-compose.yml to install and start NGinX Proxy Manager"
        echo ""
        echo ""

        sudo docker compose up -d

        echo "    3. You can find NGinX Proxy Manager files at ./docker/nginx-proxy-manager"
        echo ""
        echo "    Navigate to your server hostname / IP address on port 81 to setup"
        echo "    NGinX Proxy Manager admin account."
        echo ""
        echo "    The default login credentials for NGinX Proxy Manager are:"
        echo "        IP Address : http://$(hostname -I | awk '{print $1}'):81"
        echo "        Username   : admin@example.com"
        echo "        Password   : changeme"
        echo ""       
        sleep 3s
        cd
    fi